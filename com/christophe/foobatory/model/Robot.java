package com.christophe.foobatory.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.christophe.foobartory.helpers.Pairs;

public class Robot {
	private int id;
	private Warehouses warehouses;
	private String last_action;
	
	
	
	public Robot() {
		super();
	}

	public Robot(int id, Warehouses warehouses, String last_action) {
		super();
		this.id = id;
		this.warehouses = warehouses;
		this.last_action = last_action;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Warehouses getWarehouses() {
		return warehouses;
	}

	public void setWarehouses(Warehouses warehouses) {
		this.warehouses = warehouses;
	}

	public String getLast_action() {
		return last_action;
	}

	public void setLast_action(String last_action) {
		this.last_action = last_action;
	}

	public double generateRandom(double leftLimit,double rightLimit) {
	    double generatedLong = leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
	    return generatedLong;
	}
	
	public void get_busy_for(double mining_duration) {
		double res = mining_duration*0.1;
		try {
			TimeUnit.SECONDS.sleep((long) res);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean execute_action_or_move(String activity) {
		if (activity == "mine_foo" && Arrays.asList(null, "mine_foo", "move" ).contains(this.getLast_action())) {
			this.setLast_action("mine_foo");
		    this.get_busy_for(1);
		    return true;
		}else if (activity == "mine_bar" && Arrays.asList(null, "mine_bar", "move" ).contains(this.getLast_action())) {
			this.setLast_action("mine_bar");
			double mining_duration = generateRandom(0.5,2);
		    this.get_busy_for(mining_duration);
		    return true;
		}else if (activity == "assemble_foobar" && Arrays.asList(null, "assemble_foobar", "move" ).contains(this.getLast_action())) {
			this.setLast_action("assemble_foobar");
		    this.get_busy_for(2);
		    return true;
		}else if (activity == "sell_foobars" && Arrays.asList(null, "sell_foobars", "move" ).contains(this.getLast_action())) {
			this.setLast_action("sell_foobars");
		    this.get_busy_for(10);
		    return true;
		}else if (activity == "buy_robot" && Arrays.asList(null, "buy_robot", "move" ).contains(this.getLast_action())) {
			this.setLast_action("buy_robot");
		    this.get_busy_for(0);
		    return true;
		}else {
			this.setLast_action("move");
		    this.get_busy_for(5);
		    return false;
		}
	}
	
	public void mine_foo() {
		if(execute_action_or_move("mine_foo")) {
			this.warehouses.setFoo(UUID.randomUUID().toString());			
		}
	}
	
	public void mine_bar() {
		if(execute_action_or_move("mine_bar")) {
			this.warehouses.setBar(UUID.randomUUID().toString());
		}
	}
	
	public void assemble_foobars() {
		Random rand = new Random();
		if(execute_action_or_move("assemble_foobar")) {
			ArrayList<String> fooList = this.warehouses.getFoo();
			String foo = fooList.remove(fooList.size()-1);
			if(rand.nextInt(100) >= 60) {
				ArrayList<String> barList = this.warehouses.getBar();
				String bar = barList.remove(barList.size()-1);

				this.warehouses.setFoobar(new Pairs<String, String>(foo, bar));
			}
		}
	}
	
	public void sell_foobars() {
		if (execute_action_or_move("sell_foobars")) {
			int sold_foobars = Math.min(this.warehouses.getFoobar().size(), 5);
			for(int i=0;i<= sold_foobars;i++) {
				ArrayList<Pairs<String,String>> foobarList = this.warehouses.getFoobar();
				foobarList.remove(foobarList.size()-1);
			}
			double money = this.warehouses.getMoney();
			double res_money = money + sold_foobars;
			this.warehouses.setMoney(res_money);;
		}
	}
	
	
	public void buy_robot() {
		if(execute_action_or_move("buy_robot")) {
			double money = this.warehouses.getMoney();
			this.warehouses.setMoney(money - 3);
			ArrayList<String> fooList = this.warehouses.getFoo();
			
			for(int i=0;i<= 6;i++) {
				fooList.remove(fooList.size()-1);
			}
			Robot robot= new Robot();
			robot.setWarehouses(this.warehouses);
			this.warehouses.setRobot(robot);
		}
	}
	
	
	public boolean is_foo_stock_low() {
		return this.warehouses.getFoo().size() <= 5;
	}
	
	public boolean is_bar_stock_low() {
		return this.warehouses.getBar().size() == 0;
	}
	
	public boolean can_assemble_foobars() {
		if(this.is_foo_stock_low())
			return false;
		else if (this.is_bar_stock_low())
			return false;
		else return true;
	}
	
	public boolean can_sell_foobars() {
		return this.warehouses.getFoobar().size() >= 5;
	}
	
	public boolean can_buy_robot() {
		if(this.warehouses.getMoney()>=3 && this.warehouses.getFoo().size() >=6) {
			return true;
		}
		return false;
	}
	
	public void next_action() {
		if(this.can_buy_robot())
			 this.buy_robot();
		else if(this.can_sell_foobars())
			 this.can_sell_foobars();
		else if(this.can_assemble_foobars())
			 this.assemble_foobars();
		else if(this.is_bar_stock_low())
			 this.mine_bar();
		else if(this.is_foo_stock_low())
			this.mine_foo();
	}

}
