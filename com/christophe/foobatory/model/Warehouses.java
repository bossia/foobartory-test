package com.christophe.foobatory.model;

import java.util.ArrayList;

import com.christophe.foobartory.helpers.Pairs;

public class Warehouses {
	private ArrayList<String> foo;
	private ArrayList<String> bar;
	private ArrayList<Pairs<String,String>> foobar;
	private double money;
	private ArrayList<Robot> robot;
	
	public ArrayList<Robot> getRobot() {
		return robot;
	}
	public void setRobot(Robot robot) {
		this.robot.add(robot);
	}
	public Warehouses() {
		super();
	}
	
	public Warehouses(ArrayList<String> foo, ArrayList<String> bar, ArrayList<Pairs<String, String>> foobar,
			double money, ArrayList<Robot> robot) {
		super();
		this.foo = foo;
		this.bar = bar;
		this.foobar = foobar;
		this.money = money;
		this.robot = robot;
	}
	public ArrayList<String> getFoo() {
		return foo;
	}
	public void setFoo(String foo) {
		this.foo.add(foo);
	}
	public ArrayList<String> getBar() {
		return bar;
	}
	public void setBar(String bar) {
		this.bar.add(bar);
	}
	
	public ArrayList<Pairs<String, String>> getFoobar() {
		return foobar;
	}
	public void setFoobar(Pairs<String, String> foobar) {
		this.foobar.add(foobar);
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	
	
}
