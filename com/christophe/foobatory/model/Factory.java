package com.christophe.foobatory.model;

import com.christophe.foobartory.helpers.Common;

public class Factory {
	 private Warehouses warehouses;
	 
	 public Factory() {
		super();
	}

	public Factory(Warehouses warehouses) {
			super();
			this.warehouses = warehouses;
		}

	public Warehouses getWarehouses() {
		return warehouses;
	}

	public void setWarehouses(Warehouses warehouses) {
		this.warehouses = warehouses;
	}

	public void display_stats() {
		System.out.println("-------------");
		System.out.println("robots: " + this.warehouses.getRobot().size());
		System.out.println("money: "+ this.warehouses.getMoney());
		System.out.println("foo: " + this.warehouses.getFoo().size());
		System.out.println("bar: " + this.warehouses.getBar().size());
		System.out.println("foobar: " + this.warehouses.getFoobar().size());
	}
	
	public void working() {
		for(int i=0; i<=Common.INITIAL_NB_ROBOTS;i++) {
			Robot robot= new Robot();
			robot.setWarehouses(this.warehouses);
			this.warehouses.setRobot(robot);
		}
		
		while(this.warehouses.getRobot().size() < Common.FINAL_NB_ROBOTS) {
			for(Robot robot: this.warehouses.getRobot()) {
				robot.next_action();
				this.display_stats();
			}
			if(this.warehouses.getRobot().size() == Common.FINAL_NB_ROBOTS)
				break;
				
		}
	}
	
	 
}
