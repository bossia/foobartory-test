package com.christophe.foobartory;

import com.christophe.foobatory.model.Factory;

public class Main {

	public static void main(String[] args) {
		Factory factory = new Factory();
		factory.working();
	}

}
